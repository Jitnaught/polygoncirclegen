@tool
extends HBoxContainer

class_name GenUI

var poly: CSGPolygon3D

func show_ui(poly: CSGPolygon3D):
	self.poly = poly
	show()

func _on_gen_button_pressed():
	var n: int = $NumPoints.value
	var hollow = $Hollow.button_pressed
	var thick = $Thickness.value
	var radius: float = $Radius.value
	
	var vec = Vector2(radius, 0)
	var angle = PI * 2 / n
	
	# add one to # points otherwise there will be a gap in the circle
	# must do after angle calc
	if hollow: n = n + 1 
	
	var points = Array()
	
	# loop from 0 to number of points-1
	for i in n:
		points.push_back(vec)
		vec = vec.rotated(angle)
	
	if hollow:
		vec = Vector2(radius - thick, 0) # thickness goes inward
		
		for i in n:
			vec = vec.rotated(-angle)
			points.push_back(vec)
	
	var packed_vecs = PackedVector2Array(points)
	poly.polygon = packed_vecs


func _on_hollow_toggled(toggled_on):
	$Thickness.visible = toggled_on
