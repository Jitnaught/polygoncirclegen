@tool
extends EditorPlugin

var ui : GenUI

func selection_changed():
	var selection = get_editor_interface().get_selection().get_selected_nodes()
	
	if selection.size() == 1 and selection[0] is CSGPolygon3D:
		ui.show_ui(selection[0])
	else:
		ui.hide()

func _enter_tree():
	ui = preload("res://addons/polygoncirclegen/ui.tscn").instantiate()
	add_control_to_container(EditorPlugin.CONTAINER_SPATIAL_EDITOR_MENU, ui)
	ui.hide()
	
	get_editor_interface().get_selection().selection_changed.connect(self.selection_changed)

func _exit_tree():
	remove_control_from_container(EditorPlugin.CONTAINER_SPATIAL_EDITOR_MENU, ui)
	if ui: ui.free()
